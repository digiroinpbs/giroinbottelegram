package id.corechain

import com.github.salomonbrys.kotson.get
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.mashape.unirest.http.Unirest
import spark.Spark.*

fun main(args: Array<String>) {
    port(7011)
    post("/api/bot/webhook") { req, res ->
        var body:String = req.body()
        val json: JsonObject
        val parser: JsonParser = JsonParser()
        try{

            json = parser.parse(body) as JsonObject
            var textSplit= json.get("message").get("text").asString.split(" ")
            var clientID =json.get("message").get("chat").get("id")
            var type =textSplit[0]
            if(type.equals("/register")){
                if(textSplit.size==4){
                    sendMessage("register with cashtag : "+ textSplit[1],clientID.toString())
                }else{
                    sendMessage("invalid parameter",clientID.toString())
                }
            }else if(type.equals("/saldo")){
                if(textSplit.size==1){
                    sendMessage("IDR 27.000,00",clientID.toString())
                }else{
                    sendMessage("invalid parameter",clientID.toString())
                }
            }else if(type.equals("/info")){
                if(textSplit.size==1){
                    sendMessage("Discount 20% Starbuck",clientID.toString())
                }else{
                    sendMessage("invalid parameter",clientID.toString())
                }
            }else if(type.equals("/topup")){
                if(textSplit.size==2){
                    var amount = textSplit[1].toInt()+1
                    sendMessage("BCA 8160xxxxxx \n" +
                            "Mandiri 8160xxxxxx \n"+
                            "Maybank 8160xxxxxx \n" +
                            "Jumlah yang harus dibayar : "+amount,clientID.toString())
                }else{
                    sendMessage("invalid parameter",clientID.toString())
                }
            }else if(type.equals("/send")){
                if(textSplit.size==3){
                    var amount = textSplit[1].toInt()
                    sendMessage("send to "+textSplit[2]+" amount: "+amount,clientID.toString())
                }else{
                    sendMessage("invalid parameter",clientID.toString())
                }
            }else if(type.equals("/cashout")){
                if(textSplit.size==4){
                    var amount = textSplit[1].toInt()
                    sendMessage("cash out "+textSplit[1]+" to "+textSplit[2]+textSplit[3], clientID.toString())
                }else{
                    sendMessage("invalid parameter",clientID.toString())
                }
            }else if(type.equals("/infobank")){
                if(textSplit.size==1){
                    sendMessage("BCA 014 \nMandiri 008 \nMaybank 016 \n",clientID.toString())
                }else{
                    sendMessage("invalid parameter",clientID.toString())
                }
            }else{
                sendMessage("invalid parameter",clientID.toString())
            }
        }catch (e:Exception){
            e.printStackTrace()
            print("API Problem")
        }
        return@post "ok"
    }
}

fun sendMessage(message:String,clientID:String){
    Unirest.post("https://api.telegram.org/bot430785378:AAGg72luoGjd_uFknn4U8c0tzmxG4Nh4TyI/sendMessage").field("text", message)
            .field("chat_id", clientID).asString()
}
